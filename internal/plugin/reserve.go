package plugin

import (
	"fmt"
	"strings"

	"github.com/hashicorp/nomad/plugins/device"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/CarbonCollins/nomad-usb-device-plugin/configs"
)

func (d *UsbDevicePlugin) doReserve(deviceIds []string) (*device.ContainerReservation, error) {
	if len(deviceIds) == 0 {
		return &device.ContainerReservation{}, nil
	}

	d.deviceLock.RLock()
	var notExistingIDs []string
	for _, id := range deviceIds {
		if _, deviceIDExists := d.devices[id]; !deviceIDExists {
			notExistingIDs = append(notExistingIDs, id)
		}
	}
	d.deviceLock.RUnlock()
	if len(notExistingIDs) != 0 {
		return nil, &reservationError{notExistingIDs}
	}

	// initialize the response
	resp := &device.ContainerReservation{
		Envs:    map[string]string{},
		Devices: []*device.DeviceSpec{},
	}

	// Provides an environment variable filled with all available deviceIds
	resp.Envs[configs.UsbVisibleDevices] = strings.Join(deviceIds, ",")

	for i, id := range deviceIds {
		if _, ok := d.devices[id]; !ok {
			return nil, status.Newf(codes.InvalidArgument, "unknown device %q", id).Err()
		}

		devMountPath := fmt.Sprintf(configs.UsbBusDevPath, d.devices[id].Bus, d.devices[id].Address)

		// Envs are a set of environment variables to set for the task.
		resp.Envs[fmt.Sprintf(configs.UsbDeviceId, i)] = id

		if d.shouldMountDevNodes == true {
			// Devices are the set of devices to mount into the container.
			resp.Devices = append(resp.Devices, &device.DeviceSpec{
				TaskPath: devMountPath,
				HostPath: devMountPath,
			})
		}
	}

	return resp, nil
}
