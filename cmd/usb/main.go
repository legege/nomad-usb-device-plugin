package main

import (
	log "github.com/hashicorp/go-hclog"
	"github.com/hashicorp/nomad/plugins"

	devicePlugin "gitlab.com/CarbonCollins/nomad-usb-device-plugin/internal/plugin"
)

func main() {
	plugins.Serve(factory)
}

func factory(log log.Logger) interface{} {
	return devicePlugin.NewUsbDevicePlugin(log)
}
