## Summary

(Summarise the bug encountered concisely)

## System Info

- Nomad version: (e.g. 1.2.0)

- OS: (e.g. ubuntu 21.04)

- Architecture: (e.g. linux/amd64)

## Steps to reproduce

(How one can reproduce the issue)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Other details

(place anything that does not fit in the above categories here)

/label ~Bug 
/assign @CarbonCollins
