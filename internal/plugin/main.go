package plugin

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	log "github.com/hashicorp/go-hclog"
	"github.com/hashicorp/nomad/plugins/base"
	"github.com/hashicorp/nomad/plugins/device"
	"github.com/hashicorp/nomad/plugins/shared/hclspec"

	"gitlab.com/CarbonCollins/nomad-usb-device-plugin/configs"
	"gitlab.com/CarbonCollins/nomad-usb-device-plugin/internal/usb"
)

var (
	ErrPluginDisabled = fmt.Errorf("USB device plugin is not enabled")
)

type UsbDevicePlugin struct {
	// enabled indicates whether the plugin should be enabled
	enabled bool

	// determins if plugin should mount the dev node into containers
	shouldMountDevNodes bool

	// A list of VIDs that will be included / excluded from the device search. Exclusions will take priority
	excludedVendorIds usb.IDList
	includedVendorIds usb.IDList

	// A list of PIDs that will be included / excluded from the device search. Exclusions will take priority
	excludedProductIds usb.IDList
	includedProductIds usb.IDList

	// fingerprintPeriod is how often we should check for devices
	fingerprintPeriod time.Duration

	// devices is the set of detected eligible devices
	devices    map[string]usb.Device
	deviceLock sync.RWMutex

	logger log.Logger
}

func NewUsbDevicePlugin(log log.Logger) *UsbDevicePlugin {
	return &UsbDevicePlugin{
		logger:             log.Named(configs.PluginID.Name),
		excludedVendorIds:  make(usb.IDList),
		includedVendorIds:  make(usb.IDList),
		excludedProductIds: make(usb.IDList),
		includedProductIds: make(usb.IDList),
		devices:            make(map[string]usb.Device),
	}
}

func (d *UsbDevicePlugin) PluginInfo() (*base.PluginInfoResponse, error) {
	return configs.PluginInfo, nil
}

func (d *UsbDevicePlugin) ConfigSchema() (*hclspec.Spec, error) {
	return configs.PluginConfigSpec, nil
}

func (d *UsbDevicePlugin) SetConfig(cfg *base.Config) error {
	return d.doSetConfig(cfg)
}

func (d *UsbDevicePlugin) Fingerprint(ctx context.Context) (<-chan *device.FingerprintResponse, error) {
	if !d.enabled {
		return nil, ErrPluginDisabled
	}

	outCh := make(chan *device.FingerprintResponse)
	go d.doFingerprint(ctx, outCh)

	return outCh, nil
}

func (d *UsbDevicePlugin) Stats(ctx context.Context, interval time.Duration) (<-chan *device.StatsResponse, error) {
	if !d.enabled {
		return nil, ErrPluginDisabled
	}

	outCh := make(chan *device.StatsResponse)
	go d.doStats(ctx, outCh, interval)

	return outCh, nil
}

type reservationError struct {
	notExistingIDs []string
}

func (e *reservationError) Error() string {
	return fmt.Sprintf("unknown device IDs: %s", strings.Join(e.notExistingIDs, ","))
}

func (d *UsbDevicePlugin) Reserve(deviceIds []string) (*device.ContainerReservation, error) {
	if !d.enabled {
		return nil, ErrPluginDisabled
	}

	return d.doReserve(deviceIds)
}
