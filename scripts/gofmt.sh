#!/usr/bin/env bash

# Check go fmt
echo "==> Fixing source code with gofmt..."
gofmt -s -w ./...

exit 0
