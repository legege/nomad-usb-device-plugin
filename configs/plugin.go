package configs

import (
	"github.com/google/gousb"
	"github.com/hashicorp/nomad/helper/pluginutils/loader"
	"github.com/hashicorp/nomad/plugins/base"
	"github.com/hashicorp/nomad/plugins/device"
	"github.com/hashicorp/nomad/plugins/shared/hclspec"
)

const (
	pluginName    = "usb"
	pluginVersion = "v0.3.1"
)

// Uniqe identifier so that Nomad can ID this plugin
var PluginID = loader.PluginID{
	Name:       pluginName,
	PluginType: base.PluginTypeDevice,
}

// Basic information about this plugin
var PluginInfo = &base.PluginInfoResponse{
	Name:              pluginName,
	Type:              base.PluginTypeDevice,
	PluginVersion:     pluginVersion,
	PluginApiVersions: []string{device.ApiVersion010},
}

// Defines the structure in which the plugin config will follow
// The codec values should match up with the PluginConfigSpec definition below
type PluginConfig struct {
	Enabled             bool       `codec:"enabled"`
	ShouldMountDevNodes bool       `codec:"mount_dev_nodes"`
	ExcludedVendorIds   []gousb.ID `codec:"excluded_vendor_ids"`
	IncludedVendorIds   []gousb.ID `codec:"included_vendor_ids"`
	ExcludedProductIds  []gousb.ID `codec:"excluded_product_ids"`
	IncludedProductIds  []gousb.ID `codec:"included_product_ids"`
	FingerprintPeriod   string     `codec:"fingerprint_period"`
}

// Defined the HCL specification to allow validation on the plugin configuration.
// The keys in this map should match up with the codec keys in the PluginConfig type above
var PluginConfigSpec = hclspec.NewObject(map[string]*hclspec.Spec{
	"enabled": hclspec.NewDefault(
		hclspec.NewAttr("enabled", "bool", false),
		hclspec.NewLiteral("true"),
	),
	"mount_dev_nodes": hclspec.NewDefault(
		hclspec.NewAttr("mount_dev_nodes", "bool", false),
		hclspec.NewLiteral("false"),
	),
	"excluded_vendor_ids": hclspec.NewDefault(
		hclspec.NewAttr("excluded_vendor_ids", "list(number)", false),
		hclspec.NewLiteral("[]"),
	),
	"included_vendor_ids": hclspec.NewDefault(
		hclspec.NewAttr("included_vendor_ids", "list(number)", false),
		hclspec.NewLiteral("[]"),
	),
	"excluded_product_ids": hclspec.NewDefault(
		hclspec.NewAttr("excluded_product_ids", "list(number)", false),
		hclspec.NewLiteral("[]"),
	),
	"included_product_ids": hclspec.NewDefault(
		hclspec.NewAttr("included_product_ids", "list(number)", false),
		hclspec.NewLiteral("[]"),
	),
	"fingerprint_period": hclspec.NewDefault(
		hclspec.NewAttr("fingerprint_period", "string", false),
		hclspec.NewLiteral("\"1m\""),
	),
})
