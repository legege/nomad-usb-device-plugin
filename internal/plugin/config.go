package plugin

import (
	"fmt"
	"time"

	"github.com/google/gousb"
	"github.com/hashicorp/nomad/plugins/base"

	"gitlab.com/CarbonCollins/nomad-usb-device-plugin/configs"
)

func (d *UsbDevicePlugin) doSetConfig(cfg *base.Config) error {
	var config configs.PluginConfig

	if len(cfg.PluginConfig) != 0 {
		if err := base.MsgPackDecode(cfg.PluginConfig, &config); err != nil {
			return err
		}
	}

	d.enabled = config.Enabled
	d.shouldMountDevNodes = config.ShouldMountDevNodes

	// VID include/exclude lists
	for _, excludedVendorId := range config.ExcludedVendorIds {
		d.excludedVendorIds[gousb.ID(excludedVendorId)] = struct{}{}
	}

	for _, includedVendorId := range config.IncludedVendorIds {
		includedVendorId = gousb.ID(includedVendorId)
		if _, notExcluded := d.excludedVendorIds[includedVendorId]; !notExcluded {
			d.includedVendorIds[includedVendorId] = struct{}{}
		}
	}

	// PID include/exclude lists
	for _, excludedProductId := range config.ExcludedProductIds {
		d.excludedProductIds[gousb.ID(excludedProductId)] = struct{}{}
	}

	for _, includedProductId := range config.IncludedProductIds {
		includedProductId = gousb.ID(includedProductId)
		if _, notExcluded := d.excludedProductIds[includedProductId]; !notExcluded {
			d.includedProductIds[includedProductId] = struct{}{}
		}
	}

	// Fingerprint timings
	period, err := time.ParseDuration(config.FingerprintPeriod)
	if err != nil {
		return fmt.Errorf("failed to parse doFingerprint period %q: %v", config.FingerprintPeriod, err)
	}
	d.fingerprintPeriod = period

	return nil
}
