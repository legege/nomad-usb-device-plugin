# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed
- ([#23](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/23)) Refactored project structure to align with golang standards
- ([#15](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/15)) Update golang to 1.18
- ([#22](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/22)) Update google/gousb to 1.1.2
- ([#24](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/24)) Update hashicorp/nomad to 1.3.1

### Removed
- Dependency to forked and outdated package github.com/hashicorp/gopsutil
- Dependency to forked and outdated package github.com/hashicorp/go-msgpack

### Fixed

- ([#25](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/25)) Prevents Devices with the same VID:PID but with differing serial numbers from being grouped together and sharing attributes

## [0.3.1] - 2022-07-02

### Fixed
- ([#21](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/21)) Correct driver version when reporting to Nomad


## [0.3.0] - 2022-07-01

### Added
- ([#16](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/16)) Serial is now reported as serial respectivly to nomad (Thanks [@legege](https://gitlab.com/legege)!)

### Changed
- ([#17](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/17)) Changelog now uses the [Keep a Changelog](https://keepachangelog.com) format

### Fixed
- ([#19](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/19)) Use release syntax in pipeline yaml when tagging


## [0.2.0] - 2021-01-06

### Added
- ([#3](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/3)) Now possible to filter based on product id
- ([#4](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/4)) Plugin stanza option to automatically mount dev nodes

### Changed
- ([#5](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/5)) Vendor and model are now reported as VID and PID respectivly to nomad 
- ([#5](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/5)) Available attributes have been changed to be more consistant with eachother

### Fixed
- ([#6](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/6)) Fingerprinting should now be able to check if a USB has been changed on the system


## [0.1.0] - 2021-01-04
Initial release of USB Device Driver

### Added
- ([#2](https://gitlab.com/CarbonCollins/nomad-usb-device-plugin/-/issues/2)) Allows Nomad agent to include or exclude specific USB vendor ids from fingerprinting
- Allows Nomad task to add constraints on a USB device with an of the following attributes:
  - `vendor_id`
  - `product_id`
  - `class`
  - `sub_class`
  - `protocol`
- Builds binaries for four linux architectures that Nomad currently supports (amd64, i386, armv7, and arm64)
