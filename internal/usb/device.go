package usb

import (
	"github.com/google/gousb"
)

type Device struct {
	VID      gousb.ID
	PID      gousb.ID
	Class    gousb.Class
	SubClass gousb.Class
	Protocol gousb.Protocol

	Bus     int
	Address int
	Port    int
	Serial  string
}
